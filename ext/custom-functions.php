<?php

	// Our hooked in function - $fields is passed via the filter!
	// function custom_override_checkout_fields( $fields ) {

	// 	/*$fields['billing']['billing_phone']['label'] = 'เบอร์โทรศัพท์';
	// 	$fields['billing']['billing_phone']['placeholder'] = '';

	// 	$fields['billing']['billing_phone']['label'] = 'เบอร์โทรศัพท์';
	// 	$fields['billing']['billing_phone']['placeholder'] = '';*/

	// 	$fields['billing']['billing_first_name'] = array(
	// 		'label' => "ชื่อ-สกุล",
	// 		'required' => true
	// 		);

	// 	unset($fields['billing']['billing_address_1']);
	// 	$fields['billing']['billing_address_1'] = array(
	// 		'label' => "ที่อยู่",
	// 		'required' => true,
	// 		'type' => "textarea",
	// 		'class' => array('notes'),
	// 		);

	// 	/*$postcode = $fields['billing']['billing_postcode'];
	// 	unset($fields['billing']['billing_postcode']);
	// 	$fields['billing']['billing_postcode'] = $postcode;

	// 	$fields['billing']['billing_postcode'] = array(
	// 		'label' => "รหัสไปรษณีย์",
	// 		'required' => true,
	// 		'class' => array('form-row-wide', 'address-field'),
	// 		'clear' => 1,
	// 		'validate' => array('postcode'),
	// 		);*/

	// 	unset($fields['billing']['billing_city']);
	// 	/*$fields['billing']['billing_city'] = array(
	// 		'label' => "เขต",
	// 		'required' => true,
	// 		);*/


	// 	$state = $fields['billing']['billing_state'];
	// 	unset($fields['billing']['billing_state']);
	// 	$fields['billing']['billing_state'] = array(
	// 		'type' => "state",
	// 		'label' => "รหัสไปรษณีย์",
	// 		'placeholder' => "",
	// 		'required' => true,
	// 		'class' => array('form-row-wide', 'address-field'),
	// 		'clear' => 1,
	// 		'validate' => array('state'),
	// 		);

	// 	$country = $fields['billing']['billing_country'];
	// 	unset($fields['billing']['billing_country']);
	// 	$fields['billing']['billing_country'] = $country;		

	//     // unset($fields['billing']['billing_first_name']);
	//     unset($fields['billing']['billing_last_name']);
	//     unset($fields['billing']['billing_company']);	    
	//     unset($fields['billing']['billing_address_2']);  
	//     // unset($fields['billing']['billing_country']);
	//     // unset($fields['billing']['billing_state']);
	//     // unset($fields['billing']['billing_phone']);
	//     // unset($fields['order']['order_comments']);
	//     // unset($fields['billing']['billing_address_2']);
	//     // unset($fields['billing']['billing_postcode']);
	//     // unset($fields['billing']['billing_company']);
	//     // unset($fields['billing']['billing_last_name']);
	//     // unset($fields['billing']['billing_email']);
	//     // unset($fields['billing']['billing_city']);	

	// 	unset($fields['order']['order_comments']);
	// 	$fields['order']['order_comments'] = array(
	// 		'type' => "textarea",
	// 		'class' => array("notes"),
	// 		'label' => "รายละเอียด",
	// 		"placeholder" => "รายละเอียดเพิ่มเติม เกี่ยวกับข้อมูลสั่งซื้อของคุณ",
	// 		);	  

	// 	unset($fields['account']['account_password']);
	// 	$fields['account']['account_password'] = array(
	// 		'type' => "password",
	// 		'label' => "รหัสผ่าน",
	// 		'required' => 1,
	// 		"placeholder" => "",
	// 		);				  	    
	    	
	// 	// echo "<pre>";
	// 	// print_r($fields);
	// 	// echo "</pre>";

	// 	return $fields;
	// }

	// // Hook in
	// add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

	function translate_text($translated) {
		// Login / Register
		$translated = str_ireplace('Lost password?', 'ลืมรหัสผ่าน ?', $translated);
		$translated = str_ireplace('Lost password', 'ลืมรหัสผ่าน', $translated);
		$translated = str_ireplace('Confirm password', 'ยืนยันรหัสผ่าน', $translated);
		$translated = str_ireplace('Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', 'ลืมรหัสผ่าน ? โปรดกรอก รหัสผู้ใช้หรือ email ระบบจะทำการส่งลิงค์รีเซ็ทรหัสผ่านไปยัง email ของท่าน', $translated);
		$translated = str_ireplace('Reset Password', 'รีเซ็ทรหัสผ่าน', $translated);		
		$translated = str_ireplace('Username or email', 'รหัสผุ้ใช้ หรือ email', $translated);		

		// Checkout
		$translated = str_ireplace('Create an account by entering the information below. If you are a returning customer please login at the top of the page.', 'ถ้าคุณต้องการสร้างบัญชีพร้อมกับสั่งซื้อครั้งนี้ โปรดกรอกรหัสผ่านด้านล่าง', $translated);
		$translated = str_ireplace('Billing Details', 'ที่อยู่ในการจัดส่ง', $translated);
		$translated = str_ireplace('Additional Information', 'ข้อมูลเพิ่มเติม', $translated);		
		$translated = str_ireplace('Telephone', 'เบอร์โทรศัพท์', $translated);
		$translated = str_ireplace('Phone', 'เบอร์โทรศัพท์', $translated);
		$translated = str_ireplace('Addresses', 'ที่อยู่', $translated);
		$translated = str_ireplace('Address', 'ที่อยู่', $translated);
		$translated = str_ireplace('State / County', 'จังหวัด', $translated);
		$translated = str_ireplace('Postcode / Zip', 'รหัสไปรษณีย์', $translated);				
		$translated = str_ireplace('Country', 'ประเทศ', $translated);		
		$translated = str_ireplace('Order Notes', 'รายละเอียด', $translated);		
		$translated = str_ireplace('Payment Methods', 'เลือกวิธีการชำระเงิน', $translated);		
		$translated = str_ireplace('Create an account?', 'สร้างบัญชี ?', $translated);
		$translated = str_ireplace('Account password', 'รหัสผ่าน', $translated);

		$translated = str_ireplace('First name', 'ชื่อ-สกุล', $translated);
		$translated = str_ireplace('Username', 'รหัสผู้ใช้', $translated);
		$translated = str_ireplace('Password', 'รหัสผ่าน', $translated);		
		$translated = str_ireplace('Signup', 'สมัครสมาชิก', $translated);		
		$translated = str_ireplace('Login', 'เข้าสู่ระบบ', $translated);		
		$translated = str_ireplace('Register', 'สมัครสมาชิก', $translated);	
		$translated = str_ireplace('This field is required.', 'กรุณากรอก', $translated);	

		// Top Menu
		$translated = str_ireplace('My Account', 'บัญชีของฉัน', $translated);
		$translated = str_ireplace('Log out', 'ออกจากระบบ', $translated);

		// Shop		
		$translated = str_ireplace('Cart Subtotal', 'รวม', $translated);
		$translated = str_ireplace('Order Total', 'ยอดสุทธิ', $translated);

		$translated = str_ireplace('Filters', 'ตัวกรอง', $translated);
		$translated = str_ireplace('ALL PRODUCTS', 'สินค้าทั้งหมด', $translated);
		$translated = str_ireplace('Update Cart', 'อัพเดท', $translated);
		$translated = str_ireplace('CART', 'ตระกร้าสินค้า', $translated);
		$translated = str_ireplace('Product', 'สินค้า', $translated);
		$translated = str_ireplace('Price', 'ราคา', $translated);
		$translated = str_ireplace('Status', 'สถานะ', $translated);
		$translated = str_ireplace('Qty', 'จำนวน', $translated);
		$translated = str_ireplace('Total', 'รวม', $translated);
		$translated = str_ireplace('Remove', 'ลบ', $translated);
		$translated = str_ireplace('Coupon code', 'รหัสคูปอง', $translated);
		$translated = str_ireplace('Apply coupon', 'ใช้คูปอง', $translated);
		$translated = str_ireplace('Coupon', 'คูปอง', $translated);
		
		$translated = str_ireplace('Sort Gallery', 'อัลบั้มรีวิว', $translated);

		// Forms
		$translated = str_ireplace('The uploaded file type is not allowed', 'ประเภทของไฟล์ไม่ถูกต้อง. กรุณาอัพโหลดไฟล์', $translated);		
		$translated = str_ireplace('Please enter a valid time.', 'รูปแบบเวลาไม่ถูกต้อง', $translated);		
		$translated = str_ireplace('There was a problem with your submission. Errors have been highlighted below.', '..', $translated);


		// BLOG
		$translated = str_ireplace('BLOG', 'บทความ', $translated);		

		return $translated;
	}
	add_filter('gettext', 'translate_text');
	add_filter('ngettext', 'translate_text');


	function change_message($message, $form){
		return '
		<div class="validation_error">คุณกรอกข้อมูลไม่ถูกต้อง กรุณาตรวจสอบข้อมูลด้านล่าง</div>
		';
	}
	add_filter("gform_validation_message", "change_message", 10, 2);
	

	function custom_wc_ajax_variation_threshold( $qty, $product ) {
		return 60;
	}
	add_filter( 'woocommerce_ajax_variation_threshold', 'custom_wc_ajax_variation_threshold', 10, 2 );


	function wpb_woo_my_account_order() {
		$myorder = array(
			'edit-account'       => __( 'ข้อมูลบัญชี', 'woocommerce' ),
			'orders'             => __( 'ข้อมูลการสั่งซื้อ', 'woocommerce' ),
			'edit-address'       => __( 'ที่อยู่', 'woocommerce' ),
			'customer-logout'    => __( 'ออกจากระบบ', 'woocommerce' ),
			);
		return $myorder;
	}
	add_filter ( 'woocommerce_account_menu_items', 'wpb_woo_my_account_order' );


	function custom_override_default_address_fields( $address_fields ) {

		$address_fields['first_name']['label'] = "ชื่อ-สกุล";
		$address_fields['address_1'] = array(
			'label' => "ที่อยู่",
			'required' => true,
			'type' => "textarea",
			'class' => array('notes'),
			);

		unset($address_fields['last_name']);
		unset($address_fields['company']);
		unset($address_fields['address_2']);
		unset($address_fields['city']);

		return $address_fields;
	}
	add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );


	function fb_login_footer() {
		?>
		<a href="#" class="fb_login" onclick="checkLoginState('login');"><i class="fa fa-facebook"></i> <span>เข้าสู่ระบบ</span></a>
		<div class="status login"></div>
		<?php
	}

	add_action( 'woocommerce_after_customer_login_form', 'fb_login_footer' );


	/* AJAX */
	function fb_login_callback() {

		if ( check_ajax_referer( 'sportlifeonline-nonce', 'security' )){
			
			$email = $_POST["email"];

			if ( email_exists($email) ) {
				$user = get_user_by( 'email', $email );

				wp_set_auth_cookie( $user->ID, false );

				$result["data"] = $creds;
				$result["msg"] = "Login สำเร็จแล้ว";
				$status = true;
			}else{
				$result["msg"] = "ไม่พบอีเมลล์ ของท่านกรุณาตรวจสอบอีเมลล์";
				$status = false;
			}

			$result["status"] = $status;

			echo json_encode($result);

		}

		wp_die();
	}
	add_action( 'wp_ajax_nopriv_fb_login', 'fb_login_callback' );
	add_action( 'wp_ajax_fb_login', 'fb_login_callback' );	


	/* AJAX */
	function fb_register_callback() {

		if ( check_ajax_referer( 'sportlifeonline-nonce', 'security' )){
			
			$email = $_POST["email"];

			if ( email_exists($email) ) {
				$result["msg"] = "อีเมลล์มีอยู่ในระบบแล้ว";
				$status = false;
			}else{

				// Do register here.
				$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
            	$user_id = wp_create_user( $email, $random_password, $email ); 

				// Login
            	if ( $user_id ) {
            		wp_set_auth_cookie( $user_id, false );
            		
            		$result["msg"] = "สมัครสมาชิกสำเร็จ กำลังเข้าสู่ระบบ";
            		$status = true;
            	}else{
            		$result["msg"] = "สมัครสมาชิกไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ";
            		$status = false;
            	}
			}

			$result["status"] = $status;			

			echo json_encode($result);

		}

		wp_die();
	}
	add_action( 'wp_ajax_nopriv_fb_register', 'fb_register_callback' );
	add_action( 'wp_ajax_fb_registern', 'fb_register_callback' );		
?>