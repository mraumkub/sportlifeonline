<?php
	
	function sportlo_scripts() {	
		wp_enqueue_style( 'sportlo-css', get_template_directory_uri().'/ext/css/style.css' );
		wp_enqueue_script( 'sportlo-js', get_template_directory_uri() . '/ext/js/main.js', array('jquery'), '1.3', true );
		?>
		
		<script type="text/javascript">
			var ajaxurl = '<?php echo admin_url("admin-ajax.php"); ?>';
      		var ajax_nonce = '<?php echo wp_create_nonce("sportlifeonline-nonce") ?>';
		</script>
		<?php
	}

	add_action( 'wp_enqueue_scripts', 'sportlo_scripts' );

	// Custom Functions
	require_once( get_template_directory() . '/ext/custom-functions.php');
?>