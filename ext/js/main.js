(function($) {
	$(document).on("click", "[name='createaccount']", function(){
		var chk = $(this).is(":checked");
		$("div.create-account").toggle();
	});	

	if ($(".woocommerce-checkout").length > 0) {
		$(".woocommerce-checkout div.woocommerce form.login div.clear:last-child").after('<div class="fb_login_container"><a href="#" class="fb_login" onclick="checkLoginState(\'login\');"><i class="fa fa-facebook"></i> </span>เข้าสู่ระบบ</span></a><div class="status login"></div></div>');
	}

	$(window).load(function() {

		if ($(".pt-categories").length > 0) {
			// $(".pt-categories li a.show-children").removeClass("collapsed");
			// $(".pt-categories ul.children.collapse").addClass("in");
		}

		$(".primary-nav .suppa_menu_linksTwo").css("position", "static");

		// console.log($(".primary-nav .suppa_menu_linksTwo"));

	});
})(jQuery);

var $ = jQuery.noConflict();

function statusChangeCallback(response, action) {
	// console.log('statusChangeCallback');	
	if (response.status === 'connected') {
		doAPI(action);
	} else if (response.status === 'not_authorized') {
		$(".status").html('Please log ' + 'into this app.');
	} else {
		$(".status").html('Please log ' + 'into Facebook.');
	}
}

function checkLoginState(action) {
	FB.login(function(result) {

		if (result.status === 'connected') {

			FB.api('/me?fields=email,name', function(response) {

				if ( response.email ) {

					if ( action == "login" ) {
						var data = {
							'action': 'fb_login',
							'email' : response.email,		
							'security' : ajax_nonce,
						};   
					} else {
						var data = {
							'action': 'fb_register',
							'email' : response.email,		
							'security' : ajax_nonce,
						};   		
					}

					$(".status."+action).html("กรุณารอซักครู่...");

					$.post(
						ajaxurl, 
						data, 
						function(data) {

						if ( data.status === true ) {
							location.reload();			
						}
					
					$(".status."+action).html(data.msg);

				},"json");

				}else{
					$(".status."+action).html('ไม่พบอีเมล์');			
				}

				return false;
			});

		} else if (response.status === 'not_authorized') {
			$(".status").html('Please log ' + 'into this app.');
		} else {
			$(".status").html('Please log ' + 'into Facebook.');
		}

		

	}, { scope: 'email,public_profile' });
}

window.fbAsyncInit = function() {
	FB.init({
		appId      : '1734377646825431',
		cookie     : true,  
		xfbml      : true,
		version    : 'v2.7'
	});
};

// Load the SDK asynchronously
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function doAPI(action) {

	FB.api('/me?fields=id,email,name', function(response) {
	console.log(response);

		if ( response.email ) {

			if ( action == "login" ) {
				var data = {
					'action': 'fb_login',
					'email' : response.email,		
					'security' : ajax_nonce,
				};   
			} else {
				var data = {
					'action': 'fb_register',
					'email' : response.email,		
					'security' : ajax_nonce,
				};   		
			}

			$(".status."+action).html("กรุณารอซักครู่...");

			$.post(
				ajaxurl, 
				data, 
				function(data) {

					if ( data.status === true ) {
					// location.reload();			
				}
				
				$(".status."+action).html(data.msg);

			},"json");

		}else{
			$(".status."+action).html('ไม่พบอีเมล์');			
		}

	return false;
	});
}